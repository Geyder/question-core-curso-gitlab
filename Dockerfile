FROM registry.gitlab.com/geyder/question-core-curso-gitlab:dependencies

COPY . .

RUN mkdir -p assets/static \
    && python manage.py collectstatic --noinput